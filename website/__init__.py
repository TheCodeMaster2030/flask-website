from flask import Flask, jsonify, redirect, session, url_for, render_template, request
from dotenv import load_dotenv, find_dotenv
import stripe, json
from os import environ as env
from werkzeug.exceptions import HTTPException
from authlib.integrations.flask_client import OAuth
from six.moves.urllib.parse import urlencode
from functools import wraps
import constantsFile

ENV_FILE = find_dotenv()
if ENV_FILE:
    load_dotenv(ENV_FILE)

AUTH0_CALLBACK_URL = env.get(constantsFile.AUTH0_CALLBACK_URL)
AUTH0_CLIENT_ID = env.get(constantsFile.AUTH0_CLIENT_ID)
AUTH0_CLIENT_SECRET = env.get(constantsFile.AUTH0_CLIENT_SECRET)
AUTH0_DOMAIN = env.get(constantsFile.AUTH0_DOMAIN)
AUTH0_BASE_URL = 'https://' + AUTH0_DOMAIN
AUTH0_AUDIENCE = env.get(constantsFile.AUTH0_AUDIENCE)


def create_app():
    app = Flask(__name__)
    app.secret_key = constantsFile.SECRET_KEY
    app.debug = True

    from .views import views
    from .auth import auth

    app.register_blueprint(views, url_prefix='/')
    app.register_blueprint(auth, url_prefix='/')

    # Setup Stripe python client library
    # load_dotenv(find_dotenv())
    # For sample support and debugging, not required for production:
    stripe.set_app_info(
        'stripe-samples/checkout-single-subscription',
        version='0.0.1',
        url='https://github.com/stripe-samples/checkout-single-subscription')

    stripe.api_version = '2020-08-27'
    stripe.api_key = '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'

    oauth = OAuth(app)
    auth0 = oauth.register(
        'auth0',
        client_id=AUTH0_CLIENT_ID,
        client_secret=AUTH0_CLIENT_SECRET,
        api_base_url=AUTH0_BASE_URL,
        access_token_url=AUTH0_BASE_URL + '/oauth/token',
        authorize_url=AUTH0_BASE_URL + '/authorize',
        client_kwargs={
            'scope': 'openid profile email',
        },
    )

    @app.route('/config', methods=['GET'])
    def get_publishable_key():
        return jsonify({
            'publishableKey': 'pk_live_51JxcojKK3D090enHRUYK5SZZXdLJMd2nMPjVZdWIgdpLdTT4FWMoIaTNCaudVh8FBoof1DhM7zBlTGk2FinLjjpJ00JaHT9KQ0',
            'basicPrice': 'price_1KDandKK3D090enHW1L5AG8l',
            'proPrice': 'price_1KBP8cKK3D090enHck8jDNGJ'
        })

    # Fetch the Checkout Session to display the JSON result on the success page
    @app.route('/checkout-session', methods=['GET'])
    def get_checkout_session():
        id = request.args.get('sessionId')
        checkout_session = stripe.checkout.Session.retrieve(id)
        return jsonify(checkout_session)

    @app.route('/create-checkout-session', methods=['POST'])
    def create_checkout_session():
        price = request.form.get('priceId')
        domain_url = 'http://127.0.0.1:5000/'

        try:
            # Create new Checkout Session for the order
            # Other optional params include:
            # [billing_address_collection] - to display billing address details on the page
            # [customer] - if you have an existing Stripe Customer ID
            # [customer_email] - lets you prefill the email input in the form
            # [automatic_tax] - to automatically calculate sales tax, VAT and GST in the checkout page
            # For full details see https://stripe.com/docs/api/checkout/sessions/create

            # ?session_id={CHECKOUT_SESSION_ID} means the redirect will have the session ID set as a query param
            checkout_session = stripe.checkout.Session.create(
                success_url=domain_url + '/success.html?session_id={CHECKOUT_SESSION_ID}',
                cancel_url=domain_url + '/canceled.html',
                mode='subscription',
                # automatic_tax={'enabled': True},
                line_items=[{
                    'price': price,
                    'quantity': 1
                }],
            )
            return redirect(checkout_session.url, code=303)
        except Exception as e:
            return jsonify({'error': {'message': str(e)}}), 400

    @app.route('/customer-portal', methods=['POST'])
    def customer_portal():
        # For demonstration purposes, we're using the Checkout session to retrieve the customer ID.
        # Typically this is stored alongside the authenticated user in your database.
        checkout_session_id = request.form.get('sessionId')
        checkout_session = stripe.checkout.Session.retrieve(checkout_session_id)

        # This is the URL to which the customer will be redirected after they are
        # done managing their billing with the portal.
        return_url = 'http://127.0.0.1:5000/'

        session = stripe.billing_portal.Session.create(
            customer=checkout_session.customer,
            return_url=return_url,
        )
        return redirect(session.url, code=303)

    # @app.route('/webhook', methods=['POST'])
    # def webhook_received():
    #     # You can use webhooks to receive information about asynchronous payment events.
    #     # For more about our webhook events check out https://stripe.com/docs/webhooks.
    #     webhook_secret = 'whsec_84iLGdVvKwleObejsCLoLcUZIyvwp8rd'
    #     request_data = json.loads(request.data)
    #
    #     if webhook_secret:
    #         # Retrieve the event by verifying the signature using the raw body and secret if webhook signing is configured.
    #         signature = request.headers.get('stripe-signature')
    #         try:
    #             event = stripe.Webhook.construct_event(
    #                 payload=request.data, sig_header=signature, secret=webhook_secret)
    #             data = event['data']
    #         except Exception as e:
    #             return e
    #         # Get the type of webhook event sent - used to check the status of PaymentIntents.
    #         event_type = event['type']
    #     else:
    #         data = request_data['data']
    #         event_type = request_data['type']
    #     data_object = data['object']
    #
    #     print('event ' + event_type)
    #
    #     if event_type == 'checkout.session.completed':
    #         print('🔔 Payment succeeded!')
    #
    #     return jsonify({'status': 'success'})



    def requires_auth(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            if constantsFile.PROFILE_KEY not in session:
                return redirect('/loginAuth0')
            return f(*args, **kwargs)

        return decorated

    @app.route('/callback')
    def callback_handling():
        auth0.authorize_access_token()
        resp = auth0.get('userinfo')
        userinfo = resp.json()

        session[constantsFile.JWT_PAYLOAD] = userinfo
        session[constantsFile.PROFILE_KEY] = {
            'user_id': userinfo['sub'],
            'name': userinfo['name'],
            'picture': userinfo['picture']
        }
        return auth0.authorize_redirect(redirect_uri=AUTH0_CALLBACK_URL, audience=AUTH0_AUDIENCE)

    @app.route('/loginAuth0')
    def login():
        return auth0.authorize_redirect(redirect_uri=AUTH0_CALLBACK_URL, audience=AUTH0_AUDIENCE)

    @app.route('/dashboard')
    @requires_auth
    def dashboard():
        return render_template('dashboard.html',
                               userinfo=session[constantsFile.PROFILE_KEY],
                               userinfo_pretty=json.dumps(session[constantsFile.JWT_PAYLOAD], indent=4))

    @app.route('/logout')
    def logout():
        session.clear()
        return redirect('/')

    return app


